package com.arpit.tila.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.arpit.tila.notifiers.Loader
import com.arpit.tila.notifiers.Notifier
import com.arpit.tila.notifiers.NotifyException

open class BaseViewModel: ViewModel() {

    /**
     * Use notifier to send any message and data to the activity from ViewModel
     */
    val notifier = Notifier(viewModelScope)

    fun showProgress() {
        notifier.notify(Loader(true))
    }

    fun hideProgress() {
        notifier.notify(Loader(false))
    }

    fun showError(error: Exception) {
        notifier.notify(NotifyException(error))
    }

    override fun onCleared() {
        notifier.close()
        super.onCleared()
    }
}