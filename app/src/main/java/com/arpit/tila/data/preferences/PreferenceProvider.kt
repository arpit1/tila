package com.arpit.tila.data.preferences

import android.content.Context
import android.content.SharedPreferences

class PreferenceProvider(var context: Context) {
    private var PRIVATE_MODE = 0
    private val PREFERENCE_NAME = "frankly_media"
    private val preference: SharedPreferences
        get() =
            context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE)

    fun saveData(key: String, value: Boolean = false) {
        preference.edit().putBoolean(key, value).apply()
    }

    fun getData(key: String): Boolean? {
        return preference.getBoolean(key, false)
    }
}