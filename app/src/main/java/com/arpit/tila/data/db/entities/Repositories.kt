package com.arpit.tila.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Repositories(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    var username: String? = null,
    var name: String? = null,
    var url: String? = null,
    var sponsorUrl: String? = null,
    var avatar: String? = null,
    var repo: Repo? = null
) : Serializable

data class Repo(
    var name: String? = null,
    var description: String? = null,
    var url: String? = null
) : Serializable