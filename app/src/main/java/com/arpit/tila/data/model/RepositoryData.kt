package com.arpit.tila.data.model

data class RepositoryData(
    var username: String? = null,
    var name: String? = null,
    var url: String? = null,
    var sponsorUrl: String? = null,
    var avatar: String? = null,
    var repo: Repo? = null
)

data class Repo(
    var name: String? = null,
    var description: String? = null,
    var url: String? = null
)