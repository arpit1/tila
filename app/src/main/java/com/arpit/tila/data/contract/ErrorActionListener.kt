package com.arpit.tila.data.contract

interface ErrorActionListener {
    fun onErrorActionClicked()
}