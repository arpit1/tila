package com.arpit.tila.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.arpit.tila.data.db.dao.RepositoriesDao
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.util.Converters

@Database(
    entities = [Repositories::class],
    version = 1
)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getRepositoryDao(): RepositoriesDao

    companion object {
        //    @Volatile is used so that this variable is immediately visible to all other threads
        @Volatile
        private var instance: AppDatabase? = null
        //    it is use to check that two instances not get created for database
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "Tila.db"
            ).build()
    }
}