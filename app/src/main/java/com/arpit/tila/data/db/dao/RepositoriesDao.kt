package com.arpit.tila.data.db.dao

import androidx.room.*
import com.arpit.tila.data.db.entities.Repositories

@Dao
interface RepositoriesDao {

    @Transaction
    suspend fun updateData(repositories: ArrayList<Repositories>) {
        delete()
        insert(repositories)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(repo: ArrayList<Repositories>)

    @Query("DELETE FROM Repositories")
    suspend fun delete()

    @Query("SELECT * FROM Repositories")
    suspend fun getAllRepositories(): List<Repositories>
}