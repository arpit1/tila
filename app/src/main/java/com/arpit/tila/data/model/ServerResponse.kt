package com.arpit.tila.data.model

/**
 * It is the common response structure for all the API's.
 * result should always come in the form of JSON Array.
 */
class ServerResponse<T> {
    var error: Int? = null
    var message: String? = null
    var count: Int? = null
    var result: List<T>? = null
}