package com.arpit.tila.data.db.repository

import com.arpit.tila.data.db.AppDatabase
import com.arpit.tila.data.db.entities.Repositories

class TrendingRoomDbRepository(private val database: AppDatabase) {

    suspend fun saveRepo(data: ArrayList<Repositories>) = database.getRepositoryDao().updateData(data)

    suspend fun getRepo() = database.getRepositoryDao().getAllRepositories()
}