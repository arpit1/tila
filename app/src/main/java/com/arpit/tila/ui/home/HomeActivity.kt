package com.arpit.tila.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.arpit.tila.R
import com.arpit.tila.adapter.RepositoriesListAdapter
import com.arpit.tila.common.BaseActivity
import com.arpit.tila.common.BaseViewModel
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.databinding.ActivityMainBinding
import com.arpit.tila.notifiers.Notify
import com.arpit.tila.ui.detail.RepoDetailActivity
import com.arpit.tila.ui.home.HomeViewModel.Companion.REPO_DATA
import com.arpit.tila.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class HomeActivity : BaseActivity() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var toolbar: Toolbar
    private val binding: ActivityMainBinding by lazyBinding()
    private var sortByOptions: Array<String>? = null

    private val factory by instance<HomeViewModelFactory>()

    override val dataBinding: Boolean = true
    override val layoutResource: Int = R.layout.activity_main
    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun initializeViewModel() {
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
    }

    override fun setBindings() {
        binding.viewModel = viewModel
        binding.errorModel = viewModel.errorModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbar()

        sortByOptions = resources.getStringArray(R.array.sort_by)

        spinner_sort_by.adapter = ArrayAdapter(
            this,
            R.layout.spinner_item,
            sortByOptions!!
        )
        spinner_sort_by.onItemSelectedListener = sortBySelectedListener
        initRecyclerView()
        setObserver()
    }

    private fun setObserver() {
        viewModel.repoList.observe(this, Observer {
            if (it is ArrayList<*>) {
                val adapter = (repoListRecyclerView.adapter) as RepositoriesListAdapter
                @Suppress("UNCHECKED_CAST")
                adapter.area = it as ArrayList<Repositories>
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun initRecyclerView() {
        repoListRecyclerView.also {
            it.layoutManager = LinearLayoutManager(this)
            it.setHasFixedSize(true)
            it.adapter = RepositoriesListAdapter(ArrayList(), viewModel)
            val animator: RecyclerView.ItemAnimator = it.itemAnimator!!

            if (animator is SimpleItemAnimator) {
                animator.supportsChangeAnimations = false
            }
        }
    }

    private var sortBySelectedListener: AdapterView.OnItemSelectedListener? = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(arg0: AdapterView<*>?, arg1: View?, position: Int, id: Long) {
            if (position > 0)
                viewModel.sortBy(sortByOptions!![position])
            else {
                if (viewModel.arrRepo.size == 0)
                    viewModel.getRepoList()
                else
                    viewModel.sortBy("")
            }
        }

        override fun onNothingSelected(arg0: AdapterView<*>?) {}
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)

        toolbar.title = resources.getString(R.string.title)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        setSupportActionBar(toolbar)
    }

    override fun onNotificationReceived(data: Notify) {
        when (data.identifier) {
            REPO_DATA -> {
                hideKeyboard(binding.root)
                Intent(this, RepoDetailActivity::class.java).also {
                    it.putExtra("repoData", data.arguments[0] as Repositories)
                    startActivity(it)
                }
            }
        }
    }
}