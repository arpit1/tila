package com.arpit.tila.ui.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.arpit.tila.R
import com.arpit.tila.common.BaseActivity
import com.arpit.tila.common.BaseViewModel
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.databinding.ActivityRepoDetailBinding
import com.arpit.tila.notifiers.Notify

class RepoDetailActivity: BaseActivity() {
    private lateinit var toolbar: Toolbar
    private val binding: ActivityRepoDetailBinding by lazyBinding()
    private lateinit var repo: Repositories

    override val dataBinding: Boolean = true
    override val layoutResource: Int = R.layout.activity_repo_detail
    override fun getViewModel(): BaseViewModel? {
        return null
    }

    override fun initializeViewModel() {
    }

    override fun setBindings() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        repo = intent.getSerializableExtra("repoData") as Repositories
        binding.repo = repo

        setToolbar()
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)

        toolbar.title = repo.repo?.name
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        toolbar.setNavigationIcon(R.drawable.back_arrow_white)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item!!)
    }

    override fun onNotificationReceived(data: Notify) {

    }
}