package com.arpit.tila.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.tila.data.db.repository.TrendingRoomDbRepository
import com.arpit.tila.data.preferences.PreferenceProvider
import com.arpit.tila.network.NetworkConnectionInterceptor

class HomeViewModelFactory(
    private val roomDbRepository: TrendingRoomDbRepository,
    private val prefs: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(
            roomDbRepository,
            prefs
        ) as T
    }
}