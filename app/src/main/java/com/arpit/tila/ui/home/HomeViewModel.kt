package com.arpit.tila.ui.home

import androidx.appcompat.widget.SearchView
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.arpit.tila.common.BaseViewModel
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.data.db.repository.TrendingRoomDbRepository
import com.arpit.tila.data.model.Result
import com.arpit.tila.util.isInternetAvailable
import com.arpit.tila.data.model.ErrorModel
import com.arpit.tila.data.preferences.PreferenceProvider
import com.arpit.tila.network.NetworkConnectionInterceptor
import com.arpit.tila.network.fetchRepositoryDetails
import com.arpit.tila.network.mainScope
import com.arpit.tila.notifiers.Notify
import com.arpit.tila.util.Constant.Companion.NO_INTERNET
import com.arpit.tila.util.Constant.Companion.NO_INTERNET_CONNECTION_ERROR
import com.arpit.tila.util.Constant.Companion.ON_ERROR
import com.arpit.tila.util.Constant.Companion.RETRY
import com.arpit.tila.util.filterRepoList
import com.arpit.tila.util.showErrorModel
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class HomeViewModel(
    private val roomDbRepository: TrendingRoomDbRepository,
    private val prefs: PreferenceProvider
) : BaseViewModel() {

    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)

    var repoList: MutableLiveData<List<Repositories>> = MutableLiveData()
    var arrRepo: ArrayList<Repositories> = arrayListOf()

    fun getRepoList() {
        if (isInternetAvailable()) {
            showProgress()
            fetchRepositoryDetails(
                "java", "weekly"
            ) { result ->
                when (result) {
                    is Result.Success -> {
                        onError.set(false)
                        hideProgress()
                        prefs.saveData(DATA_SAVED, true)
                        mainScope.launch {
                            roomDbRepository.saveRepo(result.data)
                        }
                        arrRepo = result.data
                        repoList.postValue(result.data)
                    }
                    is Result.Error -> {
                        onError.set(true)
                        hideProgress()
                        result.exception.printStackTrace()
                    }
                    else -> {
                        onError.set(true)
                        hideProgress()
                    }
                }
            }
        } else {
            if (prefs.getData(DATA_SAVED)!!) {
                mainScope.launch {
                    repoList.value = roomDbRepository.getRepo()
                    arrRepo = ArrayList(repoList.value!!)
                }
            } else {
                onError.set(true)
                errorModel.showErrorModel(NO_INTERNET, NO_INTERNET_CONNECTION_ERROR, RETRY) {
                    getRepoList()
                }
            }
        }
    }

    var onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            filterUser(query)
            return true
        }

        override fun onQueryTextChange(query: String?): Boolean {
            filterUser(query)
            return false
        }
    }

    private fun filterUser(query: String?) {
        if (query != null && query.isNotEmpty()) {
            arrRepo.filterRepoList(query) { userFilteredList ->
                repoList.value = ArrayList(userFilteredList)
            }
        } else {
            repoList.postValue(arrRepo)
        }
    }

    fun sortBy(param: String) {
        when (param) {
            "Name" -> {
                arrRepo.sortWith(compareBy { it.name?.toLowerCase(Locale.ENGLISH) })
                repoList.postValue(arrRepo)
            }
            "Username" -> {
                arrRepo.sortWith(compareBy { it.username?.toLowerCase(Locale.ENGLISH) })
                repoList.postValue(arrRepo)
            }
            "Repo Name" -> {
                arrRepo.sortWith(compareBy { it.repo?.name?.toLowerCase(Locale.ENGLISH) })
                repoList.postValue(arrRepo)
            }
            else -> {
                mainScope.launch {
                    arrRepo = ArrayList(roomDbRepository.getRepo())
                    repoList.postValue(arrRepo)
                }
            }
        }
    }

    fun navigateToDetailPage(repoData: Repositories) {
        notifier.notify(Notify(REPO_DATA, repoData))
    }

    private fun onError(errorModel: ErrorModel) {
        onError.set(true)
        notifier.notify(Notify(ON_ERROR, errorModel))
    }

    companion object {
        const val DATA_SAVED = "dataSaved"
        const val REPO_DATA = "REPO_DATA"
    }
}