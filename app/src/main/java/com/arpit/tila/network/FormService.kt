package com.arpit.tila.network

import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.data.model.RepositoryData
import retrofit2.Response
import retrofit2.http.*

interface FormService {
    @GET("developers")
    suspend fun getRepositoryList(@Query("language") language: String, @Query("since") since: String): Response<ArrayList<Repositories>>
}