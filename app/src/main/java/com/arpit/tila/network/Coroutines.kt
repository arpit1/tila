package com.arpit.tila.network

import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.data.model.RepositoryData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.arpit.tila.data.model.Result
import com.arpit.tila.repository.HomeDataRepository

val ioScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
val mainScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

/**
 *
 * Fetch repository details for the given [language] & [since].
 *
 */

fun fetchRepositoryDetails(
    language: String,
    since: String,
    block: (response: Result<ArrayList<Repositories>>?) -> Unit
) {
    ioScope.launch {
        val response = HomeDataRepository(
            NetworkModule.formService
        ).getRepositoryDetails(language, since)
        block(response)
    }
}