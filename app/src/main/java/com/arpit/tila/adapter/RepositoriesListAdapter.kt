package com.arpit.tila.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.arpit.tila.R
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.databinding.RowLayoutRepoBinding
import com.arpit.tila.ui.home.HomeViewModel

class RepositoriesListAdapter (
    var area: ArrayList<Repositories>,
    private var viewModel: HomeViewModel
) : RecyclerView.Adapter<RepositoriesListAdapter.AreaListViewHolder>() {

    override fun getItemCount() = area.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AreaListViewHolder(
            DataBindingUtil.inflate (
                LayoutInflater.from(parent.context), R.layout.row_layout_repo, parent, false
            )
        )

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: AreaListViewHolder, position: Int) {
        holder.rowLayoutRepoBinding.repo = area[position]
        holder.rowLayoutRepoBinding.viewModel = viewModel
    }

    inner class AreaListViewHolder(
        val rowLayoutRepoBinding: RowLayoutRepoBinding
    ) : RecyclerView.ViewHolder(rowLayoutRepoBinding.root)
}