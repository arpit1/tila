package com.arpit.tila.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.arpit.tila.MyApplication
import com.arpit.tila.R
import com.arpit.tila.data.contract.ErrorActionListener
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.data.model.ErrorModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import java.util.*
import java.util.regex.Pattern

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.showErrorSnackBar(view: View?, msg: String) {
    try {
        if (view != null) {
            val snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_SHORT)
                .setAction("RETRY", null)
                .setBackgroundTint(ContextCompat.getColor(this, R.color.black))

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<TextView>(R.id.snackbar_text)

            textView.setTextColor(ContextCompat.getColor(this, R.color.white))
            snackbar.show()
        }
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun View.showSnackBarWithRetry(msg: String, callBack: () -> Unit) {
    try {
        val snackbar = Snackbar.make(this, msg, Snackbar.LENGTH_INDEFINITE)
            .setAction("RETRY") {
                callBack.invoke()
            }
            .setActionTextColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
            .setBackgroundTint(ContextCompat.getColor(this.context, R.color.black))
            .setTextColor(ContextCompat.getColor(this.context, R.color.white))
            .setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE)
        val button = snackbar.view.findViewById<MaterialButton>(R.id.snackbar_action)
        button.setRippleColorResource(R.color.black)
        snackbar.show()
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun Exception.logOnCrashAnalytics() {
    this.printStackTrace()
}

fun isInternetAvailable(): Boolean {
    var result = false
    val connectivityManager = MyApplication.getInstance()
        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

    connectivityManager?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    else -> false
                }
            }
        } else {
            connectivityManager.activeNetworkInfo.also { networkInfo ->
                return networkInfo != null && networkInfo.isConnected
            }
        }
    }
    return result
}

fun ErrorModel.showErrorModel(
    title: String,
    subTitle: String,
    button: String,
    action: () -> Unit
) {
    this.apply {
        errorTitle = title
        errorSubTitle = subTitle
        buttonText = button
        errorActionListener = object : ErrorActionListener {
            override fun onErrorActionClicked() {
                action()
            }
        }
    }
}

fun ArrayList<Repositories>.filterRepoList(query: String, action: (List<Repositories>) -> Unit) {
    action(this.filter {
        it.name?.toLowerCase(Locale.ENGLISH)
            ?.contains(query.toLowerCase(Locale.ENGLISH))!! ||
                it.repo?.name?.contains(query)!! ||
                it.username?.contains(query.toLowerCase(Locale.ENGLISH))!!
    })
}

fun Context.hideKeyboard(view: View?) {
    val imm: InputMethodManager? =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(view?.windowToken, 0)
}