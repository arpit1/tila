package com.arpit.tila.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.arpit.tila.R
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

object BindingUtil {

    @BindingAdapter("imageUrl", "errorDrawable", "circular")
    @JvmStatic
    fun setImage(
        imageView: ImageView,
        imageUrl: String?,
        errorDrawable: Drawable?,
        circular: Boolean = false
    ) {
        var errorDrawableImage = errorDrawable

        if (imageUrl == null || imageUrl.isEmpty()) {
            return
        }

        if (errorDrawableImage == null) {
            errorDrawableImage =
                ContextCompat.getDrawable(
                    imageView.context,
                    R.drawable.ic_image_placeholder_wrapper
                )
        }

        if (circular)
            Picasso.get().load(imageUrl).networkPolicy(NetworkPolicy.NO_CACHE)
                .error(errorDrawableImage!!).transform(CircleTransform()).into(imageView)
        else
            Picasso.get().load(imageUrl).networkPolicy(NetworkPolicy.NO_CACHE)
                .error(errorDrawableImage!!).into(imageView)
    }

    @BindingAdapter("queryTextListener")
    @JvmStatic
    fun setOnQueryTextListener(searchView: SearchView, listener: SearchView.OnQueryTextListener) {
        searchView.setOnQueryTextListener(listener)
    }
}