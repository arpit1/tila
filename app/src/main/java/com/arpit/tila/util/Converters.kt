package com.arpit.tila.util

import androidx.room.TypeConverter
import com.arpit.tila.data.db.entities.Repo
import com.google.gson.Gson

open class Converters {
    @TypeConverter
    fun setRepoObject(repoObj: Repo?): String {
        val gson = Gson()
        return gson.toJson(repoObj)
    }

    @TypeConverter
    fun getRepoObjectJson(value: String): Repo? {
        val gson = Gson()
        return gson.fromJson(value, Repo::class.java)
    }
}