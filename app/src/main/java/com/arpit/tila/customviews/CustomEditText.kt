package com.arpit.tila.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatEditText
import com.arpit.tila.R

class CustomEditText(context: Context, attrs: AttributeSet) : AppCompatEditText(context, attrs) {
    init {
        val face = Typeface.createFromAsset(context.assets, "fonts/SourceSansPro_Regular.otf")
        this.typeface = face
        this.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(R.dimen.text_size_large)
        )
    }
}