package com.arpit.tila.repository

import com.arpit.tila.common.BaseRepository
import com.arpit.tila.data.db.entities.Repositories
import com.arpit.tila.data.model.RepositoryData
import com.arpit.tila.network.FormService
import com.arpit.tila.data.model.Result

class HomeDataRepository(private val formService: FormService): BaseRepository() {

    suspend fun getRepositoryDetails(productId: String, since: String): Result<ArrayList<Repositories>>? {
        val response = safeApiCall(
            call = {
                formService.getRepositoryList(language = productId, since = since)
            },
            errorMessage = "Failed to fetch Details"
        )

        return response?.let {
            Result.Success(it)
        }
    }
}